import React, { useState, useEffect } from 'react';
import axios from 'axios';
import NO_IMAGE from './no_image_icon.png';

const country = 'in';
const apiKey = '03165693d22644ada4e7c16a17df4e9a';
const URL = `https://newsapi.org/v2/top-headlines?country=${country}&apiKey=${apiKey}`

function formatFullDateTimeToDay(dateTimeString) {
  const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  const dateTime = new Date(dateTimeString);
  const dayOfWeek = daysOfWeek[dateTime.getDay()];
  const day = String(dateTime.getDate()).padStart(2, '0');
  const month = String(dateTime.getMonth() + 1).padStart(2, '0'); // Month is 0-based
  const year = dateTime.getFullYear();
  const hours = String(dateTime.getHours()).padStart(2, '0');
  const minutes = String(dateTime.getMinutes()).padStart(2, '0');
  const seconds = String(dateTime.getSeconds()).padStart(2, '0');

  return `${dayOfWeek} ${day}/${month}/${year} ${hours}:${minutes}:${seconds}`;
}

const DataList = () => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const options = {
      method: 'GET',
      url: URL,
    };

    try {
      const response = await axios.request(options);
      setData(response.data);
      setLoading(false);
    } catch (error) {
      console.error('Error:', error);
      setLoading(false);
    }
  };

  return (
    <div>
      <div className='bg-blue-500 py-10 text-center'>
        <h1 className='text-4xl md:text-5xl font-bold text-white'>
          Top Headlines
        </h1>
        <p className='mt-4 text-lg text-white'>
          Explore our services and discover latest news headlines.
        </p>
      </div>

      {loading ? (
        <p>Loading...</p>
      ) : (
        <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4'>
          {data?.articles?.map((article, index) => (
            <div
              key={index}
              className='bg-slate-100 rounded-xl p-4 md:p-0 dark:bg-slate-800'
            >
              <div className='flex md:flex-row flex-col items-center md:space-x-4'>
                <div className='md:w-1/2'>
                  <img
                    src={article.urlToImage ? article.urlToImage : NO_IMAGE}
                    alt={article.title}
                    className='w-full h-auto rounded-md'
                  />
                </div>
                <div className='pt-4 md:pt-0 md:flex-grow'>
                  <h2 className='text-md font-medium'>{article.title}</h2>
                  <p className='text-sm font-light'>{article.description}</p>
                  <p className='text-gray-400 dark:text-sky-400 font-extralight'>
                    {formatFullDateTimeToDay(article.publishedAt)}
                  </p>
                  <p className='text-zinc-950 dark:text-slate-500'>
                    {article.source.name}
                  </p>
                </div>
              </div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default DataList;
